import {createRouter, middleware, RouterLink} from 'marionette.routing'
import Mn from 'backbone.marionette'
import Radio from 'backbone.radio'
import AdminRoute from './admin/route'
import HealthUnitsRoute from './admin/healthunits/route'
import CompaniesRoute from './admin/companies/route'
import ContractTypesRoute from './admin/contracttypes/route'
import ManagersRoute from './admin/managers/route'

let router = createRouter({log: true, logError: true})

let RootView = Mn.View.extend({
  el: '#main-wrapper',
  behaviors: [RouterLink],
  regions: {
    outlet: '#main-content'
  }
})

router.map(function (route) {
  route('admin', {path: '/', routeClass: AdminRoute, viewClass: RootView}, function () {
    route('healthunits', {routeClass: HealthUnitsRoute})
    route('companies', {routeClass: CompaniesRoute})
    route('contracttypes', {routeClass: ContractTypesRoute})
    route('managers', {routeClass: ManagersRoute})
  })
})

router.use(middleware)

Radio.channel('router').on('before:transition', function (transition) {

})

router.listen()
