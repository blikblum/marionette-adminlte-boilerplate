import {Route} from 'marionette.routing'
import View from './view'

export default Route.extend({

  activate () {

  },

  viewClass: View

})
