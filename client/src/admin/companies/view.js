import Marionette from 'backbone.marionette'
import DataBinding from 'behaviors/databinding'
import html from './template.html'

export default Marionette.View.extend({
  html: html,

  behaviors: [
    DataBinding
  ],

  events: {

  }
})
