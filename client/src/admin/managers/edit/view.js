import Marionette from 'backbone.marionette'
import DataBinding from 'behaviors/databinding'
import html from './template'

export default Marionette.View.extend({
  html: html,

  behaviors: [
    DataBinding
  ],

  events: {
    'click #cancel': 'onCancelClick'
  },

  triggers: {
    'click #save-model': 'save:model'
  },

  onCancelClick () {
    window.history.back()
  }
})
