import {Route} from 'marionette.routing'
import _ from 'underscore'
import View from './view'

export default Route.extend({

  activate (transition) {
    let managers = this.getContext().request('managers')
    this.manager = managers.get(+transition.params.managerid)
  },

  viewClass: View,

  viewOptions () {
    return {
      model: this.manager.clone()
    }
  },

  viewEvents: {
    'save:model': function (view) {
      let attributes = _.clone(view.model.attributes)
      this.manager.save(attributes, {
        success: () => {
          window.history.back()
        }
      })
    }
  }

})
