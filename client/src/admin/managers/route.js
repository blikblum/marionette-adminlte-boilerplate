import {Route} from 'marionette.routing'
import {Managers} from 'entities/manager'
import View from './view'

export default Route.extend({

  viewClass: View,

  initialize () {
    this.managers = new Managers()
  },

  activate () {
    return this.managers.fetch()
  },

  contextRequests: {
    managers: function () {
      return this.managers
    }
  }
})
