import {createRouter, middleware, RouterLink} from 'marionette.routing'
import Mn from 'backbone.marionette'
import Radio from 'backbone.radio'

let router = createRouter({log: true, logError: true})

let RootView = Mn.View.extend({
  behaviors: [RouterLink],
  regions: {
    outlet: '#main-content'
  }
})

let rootView = new RootView({
  el: '#main-wrapper'
})

router.rootRegion = rootView.getRegion('outlet')

router.map(function (route) {
  route('admin', {path: '/'}, function () {
  })
})

router.use(middleware)

Radio.channel('router').on('before:transition', function (transition) {

})

router.listen()
