import Backbone from 'backbone'

export const Manager = Backbone.Model.extend({

})

export const Managers = Backbone.Collection.extend({
  model: Manager,

  url: '../cgi-bin/outsourcingmap/data.cgi/managers'
})
