const path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: {
    app: './main.js',
    admin: './main-admin.js'
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader',
          options: {presets: [['es2015', {modules: false}]]}
        }]
      },
      {
        test: /\.html$/,
        use: [
          {loader: 'html-loader'}
        ]
      }
    ]
  },
  resolve: {
    modules: [path.resolve(__dirname, './src/common'), 'node_modules']
  },
  externals: {
    jquery: 'jQuery'
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, 'dist/index.html'),
      template: path.resolve(__dirname, 'src/index.html'),
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, 'dist/admin.html'),
      template: path.resolve(__dirname, 'src/admin.html'),
      chunks: ['admin']
    }),
    new CopyWebpackPlugin([
      {from: '../node_modules/admin-lte/dist', to: '../dist/admin-lte'},
      {from: '../node_modules/admin-lte/bootstrap', to: '../dist/bootstrap'},
      {from: '../node_modules/jquery/dist/jquery.js', to: '../dist/lib/jquery.js'}
    ])
  ]
}
