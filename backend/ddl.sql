CREATE TABLE [Company] (
  [Id] INTEGER PRIMARY KEY, 
  [Name] VARCHAR, 
  [FantasyName] VARCHAR, 
  [CNPJ] VARCHAR);


CREATE TABLE [Contract] (
  [Id] INTEGER PRIMARY KEY, 
  [UnitId] INTEGER, 
  [CompanyId] INTEGER, 
  [TypeId] INTEGER, 
  [CNPJ] VARCHAR, 
  [Value] LARGEINT, 
  [StartDate] DATE, 
  [EndDate] DATE, 
  [Historic] TEXT);


CREATE TABLE [ContractType] (
  [Id] INTEGER PRIMARY KEY, 
  [Name] VARCHAR);


CREATE TABLE [HealthUnit] (
  [Id] INTEGER PRIMARY KEY, 
  [Name] VARCHAR, 
  [ManagerId] INTEGER);


CREATE TABLE [PublicManager] (
  [Id] INTEGER PRIMARY KEY, 
  [Name] VARCHAR);


